/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moneymanagerrpl.ban;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Hydra
 */
public class MoneyManagerRPLBAN {
    
    public Connection connect() {
        String url = "jdbc:sqlite:C://Users/Hydra/Documents/rplBAN/MoneyManagerRPL-BAN.db";
        Connection conn = null;
        
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public static void createNewTable() {
        String url = "jdbc:sqlite:C://Users/Hydra/Documents/rplBAN/MoneyManagerRPL-BAN.db";
        
        String sql = "CREATE TABLE IF NOT EXISTS db_money_manager (\n"
                   + "id_ctt integer PRIMARY KEY,\n"
                   + "jenis text NOT NULL,\n"
                   + "tanggal date ,\n"
                   + "keterangan text NOT NULL,\n"
                   + "dana interger NOT NULL,\n"
                   + "id_user\n"
                   + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
        Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
    }

    public static void createNewTableuser() {
        String url = "jdbc:sqlite:C://Users/CHRISTIAN AW/Desktop/rplBAN/MoneyManagerRPL-BAN.db";
        
        String sql = "CREATE TABLE IF NOT EXISTS db_user (\n"
                   + "id_user integer PRIMARY KEY,\n"
                   + "namalengkap text NOT NULL,\n"
                   + "usernama text NOT NULL,\n"
                   + "katasandi text NOT NULL\n"
                   + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
        Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
    }
    
    public static void main(String[] args) {
        MoneyManagerRPLBAN callFunc = new MoneyManagerRPLBAN();
        
        createNewTable();
        createNewTableuser();
        
        Login masuk= new Login();
        masuk.setVisible(true);
    }
}
