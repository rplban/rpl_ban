
package moneymanagerrpl.ban;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;


public class Login extends javax.swing.JFrame {
    MoneyManagerRPLBAN koneksi = new MoneyManagerRPLBAN();
    int id_user;
    
    public Login() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tfuserName = new javax.swing.JTextField();
        login = new javax.swing.JButton();
        tpkataSandilog = new javax.swing.JPasswordField();
        jPanel2 = new javax.swing.JPanel();
        daftar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        tfnamaLengkap = new javax.swing.JTextField();
        tfnamaPengguna = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tpkataSandi = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 153, 255));

        tfuserName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        login.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        login.setText("MASUK");
        login.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        tpkataSandilog.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        daftar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        daftar.setText("DAFTAR");
        daftar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        daftar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                daftarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Nama Lengkap :");

        tfnamaLengkap.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tfnamaPengguna.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nama Pengguna :");

        tpkataSandi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tpkataSandiActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Kata Sandi :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel4.setText("Registrasi");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfnamaLengkap, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(daftar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfnamaPengguna)
                    .addComponent(tpkataSandi)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfnamaLengkap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfnamaPengguna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tpkataSandi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(daftar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Kata Sandi :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Nama Pengguna :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(331, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tpkataSandilog, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfuserName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(login)))
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfuserName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tpkataSandilog, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        tfuserName.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void setIdUser(int id){
        id_user = id;
    }
    
    public int getIdUser(){
        return id_user;
    }
    
    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        // TODO add your handling code here:\
        if (tfuserName.getText().isEmpty() || tpkataSandilog.getText().isEmpty()) {
            showMessageDialog(null, "Masukkan Nama Pengguna dan Kata Sandi Anda");
            tfuserName.setText("");
            tpkataSandilog.setText("");
        }
        
        else {
            
            String sql = "SELECT id_user,usernama,katasandi FROM db_user WHERE usernama=? AND katasandi=?";
        
             try (Connection conn = koneksi.connect();
             PreparedStatement stmtlogin  = conn.prepareStatement(sql);){
                stmtlogin.setString(1, tfuserName.getText());
                stmtlogin.setString(2, String.valueOf(tpkataSandilog.getPassword()));
                ResultSet rslogin = stmtlogin.executeQuery();
             String namaPengguna="";
             String pass="";
             int idUser=0;
             
             String namaPenggunaa=tfuserName.getText();
             String passs = String.valueOf(tpkataSandilog.getPassword());
            
             while (rslogin.next()) {   
             namaPengguna = rslogin.getString("usernama");
             pass = rslogin.getString("katasandi");
             idUser=rslogin.getInt("id_user");
            }
             setIdUser(idUser);
                 System.out.println(idUser);
                 
            if(namaPengguna.equalsIgnoreCase(namaPenggunaa) && pass.equalsIgnoreCase(passs)){
             showMessageDialog(null, "Login Sukses"); 
             HalamanBeranda hb = new HalamanBeranda();
            // hb.id_user = idUser;
             hb.setIdUser(idUser);
             hb.setVisible(true);
             this.setVisible(false);
            }
            else{
                showMessageDialog(null, "Nama Pengguna atau Kata Sandi Salah");
                tfuserName.setText("");
                tpkataSandilog.setText("");
            }
            // Tutup koneksi
                 
            rslogin.close();
            stmtlogin.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } 
    }//GEN-LAST:event_loginActionPerformed
    
    public void insert(String namaLengkap, String namaPengguna, String kataSandi)
    {
        String sql = "INSERT INTO db_user(namalengkap, usernama, katasandi)"
                + " VALUES(?,?,?)";
 
        try (Connection conn = koneksi.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
                 pstmt.setString(1, namaLengkap);
                 pstmt.setString(2, namaPengguna);
                 pstmt.setString(3, kataSandi);
            
                 pstmt.executeUpdate();
                }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public void selectAll(String namaLengkap,String namaPengguna,String kataSandi){
        String sql = "SELECT namalengkap,usernama FROM db_user WHERE namalengkap=? AND usernama=?";
        
             try (Connection conn = koneksi.connect();
             PreparedStatement stmtlogin  = conn.prepareStatement(sql);){
             stmtlogin.setString(1, tfnamaLengkap.getText());
             stmtlogin.setString(2, tfnamaPengguna.getText());
             ResultSet rslogin = stmtlogin.executeQuery();
             
             String namaLengkaap="";
             String namaPenggunna="";
                         
             while (rslogin.next()) {   
             namaLengkaap = rslogin.getString("namalengkap");
             namaPenggunna = rslogin.getString("usernama"); 
            }
                
            if(namaLengkaap.equalsIgnoreCase(namaLengkap) && namaPenggunna.equalsIgnoreCase(namaPengguna)){
             showMessageDialog(null, "Pengguna Sudah Terdaftar"); 
             tfnamaLengkap.setText("");
             tfnamaPengguna.setText("");
             tpkataSandi.setText("");
            }
            else{
                insert(namaLengkap, namaPengguna, kataSandi);
                showMessageDialog(null, "Pengguna Berhasil Ditambahkan");
                tfnamaLengkap.setText("");
                tfnamaPengguna.setText("");
                tpkataSandi.setText("");
                id_user++;
            }
            // Tutup koneksi
                 
            rslogin.close();
            stmtlogin.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
    }
    private void daftarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_daftarActionPerformed
        // TODO add your handling code here:
        int batas;
        int batas1;
        
        if (tfnamaLengkap.getText().isEmpty() && tfnamaPengguna.getText().isEmpty() && tpkataSandi.getText().isEmpty()) {
            showMessageDialog(null, "Semua field harus terisi terlebih dahulu !");
        }
        
        else {
            String namaLengkap = tfnamaLengkap.getText();
            String namaPengguna = tfnamaPengguna.getText();
            String kataSandi = tpkataSandi.getText();
            batas=kataSandi.length();
            batas1=namaPengguna.length();
            
            if (batas <6){
              showMessageDialog(null, "Kata Sandi Minimal 6 Karakter");
              tpkataSandi.setText("");
            }
            
            if (batas1 <4){
              showMessageDialog(null, "Nama Pengguna Minimal 4 Karakter");
              tfnamaPengguna.setText("");
            }
            
            else {
                selectAll(namaLengkap,namaPengguna,kataSandi);
            }
        }
    }//GEN-LAST:event_daftarActionPerformed

    private void tpkataSandiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tpkataSandiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tpkataSandiActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton daftar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton login;
    private javax.swing.JTextField tfnamaLengkap;
    private javax.swing.JTextField tfnamaPengguna;
    private javax.swing.JTextField tfuserName;
    private javax.swing.JPasswordField tpkataSandi;
    private javax.swing.JPasswordField tpkataSandilog;
    // End of variables declaration//GEN-END:variables
}
